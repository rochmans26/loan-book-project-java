package AbstractClass;

import InterfaceClass.InterBookLoan;
// import InterfaceClass.InterCalculateBookLoan;

public abstract class BookLoan extends Book implements InterBookLoan {
    private int stock;
    private double loanPrice;

    public BookLoan() {
    }

    public BookLoan(int stock, double loanPrice) {
        this.stock = stock;
        this.loanPrice = loanPrice;
    }

    public BookLoan(String bookId, String title, String author, double price, int stock, double loanPrice) {
        super(bookId, title, author, price);
        this.stock = stock;
        this.loanPrice = loanPrice;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getLoanPrice() {
        return loanPrice;
    }

    public void setLoanPrice(double loanPrice) {
        this.loanPrice = loanPrice;
    }

}
