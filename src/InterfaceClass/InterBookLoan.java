package InterfaceClass;

public interface InterBookLoan extends InterCalculateBookLoan {
    int STOCK_BENCHMARK = 10;
    Double PERCENTAGE_STOCK_LOWER_THAN_STOCK_BENCHMARK = 0.05;
    Double PERCENTAGE_STOCK_HIGHER_THAN_STOCK_BENCHMARK = 0.03;
    Double RATES_NOVEL = 0.05;
    Double RATES_COMIC = 0.1;
}
