package DisplayFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import AbstractClass.BookLoan;
import model.LoanBookOrder;
import model.Member;

public class Validation {
    static Scanner input = new Scanner(System.in);

    public static int numberValidation(String question, String errorMsg) {
        String inputAngka, regexAngka = "^[0-9]+$";

        boolean isLooping = true;
        do {
            isLooping = true;
            System.out.println(question);
            inputAngka = input.nextLine();
            // input.close();
            if (inputAngka.matches(regexAngka)) {
                isLooping = false;
            } else {
                System.out.println(errorMsg);
            }

        } while (isLooping);
        // input.close();

        return Integer.parseInt(inputAngka);
    }

    public static String memberValidation(String question, List<Member> members, String errorMsg) {
        String memberId;
        boolean isLooping = true;
        do {
            System.out.println(question);
            memberId = input.nextLine();
            List<Member> getMember = new ArrayList<Member>();
            for (Member member : members) {
                if (member.getMemberId().equalsIgnoreCase(memberId)) {
                    getMember.add(member);
                }
            }
            if (getMember.isEmpty()) {
                System.out.println();
                System.out.println(errorMsg);
                System.out.println();
            } else {
                isLooping = false;
            }

        } while (isLooping);
        return memberId;
    }

    public static String bookLoanValidation(String question, List<BookLoan> books, String errorMsg) {
        String bookID;
        boolean isLooping = true;
        do {
            System.out.println(question);
            bookID = input.nextLine();
            List<BookLoan> getBookLoan = new ArrayList<BookLoan>();
            for (BookLoan book : books) {
                if (book.getBookId().equalsIgnoreCase(bookID)) {
                    getBookLoan.add(book);
                }
            }
            if (getBookLoan.isEmpty()) {
                System.out.println();
                System.out.println(errorMsg);
                System.out.println();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        return bookID;

    }

    public static String numberOrderValidation(String question, List<LoanBookOrder> bookOrders, String errorMsg) {
        String orderID;
        boolean isLooping = true;
        do {
            System.out.println(question);
            orderID = input.nextLine();
            List<LoanBookOrder> getLoanBookOrder = new ArrayList<LoanBookOrder>();
            for (LoanBookOrder book : bookOrders) {
                if (book.getLoanID().equalsIgnoreCase(orderID)) {
                    getLoanBookOrder.add(book);
                }
            }
            if (getLoanBookOrder.isEmpty()) {
                System.out.println();
                System.out.println(errorMsg);
                System.out.println("Pilih Menu No. 4 untuk melihat daftar Book Order. Terima Kasih!");
                System.out.println();
                MainMenu.mainMenu();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        return orderID;
    }
}
