package DisplayFunction;

import java.util.List;
import java.util.Scanner;

import AbstractClass.BookLoan;
import model.LoanBookOrder;
import model.Member;

public class MainMenu {
    static Scanner input = new Scanner(System.in);

    static String memberID, bookID;
    static int lamaPinjam;

    public static void mainMenu() {
        String[] displayMainMenu = { "Data All Book For Loan", "Loan", "Return", "Data All Book Orders", "Exit" };
        int choice;
        do {
            showDisplayMenu("Main Menu", displayMainMenu);
            choice = Validation.numberValidation("Enter a number in list menu: ", "Please enter a number only..!");
            switch (choice) {
                case 0:
                    System.out.println();
                    System.out.println("Exit. Good Bye !");
                    System.out.println();
                    System.exit(0);
                    break;
                case 1:
                    System.out.println();
                    showDataAllBookForLoans(FunctionMenu.getAllBookForLoan());
                    System.out.println();
                    break;
                case 2:
                    System.out.println();
                    loan();
                    System.out.println();
                    break;
                case 3:
                    System.out.println();
                    returnBook();
                    System.out.println();
                    break;
                case 4:
                    System.out.println();
                    showDataAllBookOrders(FunctionMenu.getAllBookOrders());
                    System.out.println();
                    break;

                default:
                    System.out.println("Pilihan tidak ada dalam daftar menu ..!!");
                    break;
            }

        } while (choice != 0);
    }

    public static void returnBook() {
        String orderID;
        if (FunctionMenu.listAllBookOrder.isEmpty()) {
            System.out.println("Belum ada orderan..!!");
        } else {
            orderID = Validation.numberOrderValidation("Masukkan Nomer Order : ", FunctionMenu.listAllBookOrder,
                    "Nomor Order Tidak Ditemukan..!!");
            LoanBookOrder getBookOrder = FunctionMenu.getBookOrder(orderID);
            FunctionMenu.returnLoanBook(getBookOrder);
            System.out.println();
            System.out.println("Berhasil Return Buku..!!");
            System.out.println();
        }
    }

    public static void showDataAllBookOrders(List<LoanBookOrder> bookOrders) {
        int choice;
        do {
            PrintDataBookOrder("All Book Orders", bookOrders);
            choice = Validation.numberValidation("0 Back to Main Menu", "Hanya boleh diisi angka saja..!!");
            switch (choice) {
                case 0:
                    mainMenu();
                    break;
                default:
                    System.out.println("Enter 0 for Back to Main Menu..!");
                    break;
            }
        } while (choice != 0);
    }

    public static void showDisplayMenu(String title, String[] listMenu) {
        int number = 1;
        String formatTabel = "| %-4s | %-40s |%n";
        System.out
                .println(
                        "====================" + title + "======================");

        for (String menu : listMenu) {
            if (number == listMenu.length) {
                System.out.printf(formatTabel, 0, menu);
            } else {
                System.out.printf(formatTabel, number, menu);
            }
            number++;
        }
        System.out.println("===================================================");
    }

    public static void showDataAllBookForLoans(List<BookLoan> listBookLoans) {
        int choice;
        do {
            PrintDataBookForLoan("All Book For Loan", listBookLoans);
            choice = Validation.numberValidation("0 Back to Main Menu", "Hanya boleh diisi angka saja..!!");
            switch (choice) {
                case 0:
                    mainMenu();
                    break;
                default:
                    System.out.println("Enter 0 for Back to Main Menu..!");
                    break;
            }
        } while (choice != 0);
    }

    public static void PrintDataBookForLoan(String title, List<BookLoan> listBookLoans) {
        int number = 1;
        String formatTabel = "| %-3s | %-15s | %-40s | %-20s | %-5s |%n";

        System.out.println(
                "===================================================================================================");
        System.out.printf("%58s %n", title);
        System.out.println(
                "===================================================================================================");
        System.out.printf(formatTabel, "No.", "Book ID", "Title", "Author", "Stock");
        System.out.println(
                "===================================================================================================");

        for (BookLoan list : listBookLoans) {
            System.out.printf(formatTabel, number, list.getBookId(), list.getTitle(), list.getAuthor(),
                    list.getStock());

            number++;
        }
        System.out.println(
                "===================================================================================================");
    }

    public static void PrintDataBookOrder(String title, List<LoanBookOrder> listOrders) {
        int number = 1;
        String formatTabel = "| %-3s | %-15s | %-25s | %-15s | %-40s | %-16s | %-16s | %-10s |%n";

        System.out.println(
                "=====================================================================================================================================================================");
        System.out.printf("%90s %n", title);
        System.out.println(
                "=====================================================================================================================================================================");
        System.out.printf(formatTabel, "No.", "Loan ID", "Member Name", "Book ID", "Title", "Loan Book Price",
                "Loan Duration", "Loan Fee");
        System.out.println(
                "=====================================================================================================================================================================");

        if (listOrders.isEmpty()) {
            System.out.printf("%101s %n", "Mohon Maaf. Belum Ada Orderan ...");
        } else {
            for (LoanBookOrder list : listOrders) {
                System.out.printf(formatTabel, number, list.getLoanID(), list.getMember().getName(),
                        list.getBookloan().getBookId(), list.getBookloan().getTitle(),
                        String.format("%.1f", (list.getLoanFee() / list.getLoanDuration())), list.getLoanDuration(),
                        String.format("%.1f", list.getLoanFee()));
                number++;
            }
        }
        System.out.println(
                "=====================================================================================================================================================================");
    }

    public static void inputMenuLoan() {
        memberID = Validation.memberValidation("Masukkan Member ID : ", FunctionMenu.getAllMembers(),
                "Member Tidak Ditemukan..!!");
        bookID = Validation.bookLoanValidation("Masukkan Book ID : ", FunctionMenu.getAllBookForLoan(),
                "Buku Tidak Ditemukan..!!");
        lamaPinjam = Validation.numberValidation("Lama Pinjam : ",
                "Masukan Lama Pinjam dengan satuan hari(angka) saja ..!!");
    }

    public static void loan() {
        PrintDataBookForLoan("List Book For Loan", FunctionMenu.getAllBookForLoan());
        inputMenuLoan();
        Member member = FunctionMenu.getMember(memberID);
        BookLoan book = FunctionMenu.getBookLoan(bookID);

        String loanID = FunctionMenu.generateLoanID();
        LoanBookOrder order = new LoanBookOrder(loanID, member, book, lamaPinjam, 0);
        try {
            FunctionMenu.addLoanOrder(order);
            System.out.println();
            System.out.println("Berhasil Menambahkan Orderan..!!");
            System.out.println();
            mainMenu();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Periksa Kembali Inputan..!!");
        }

    }
}
