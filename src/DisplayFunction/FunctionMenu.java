package DisplayFunction;

import java.util.ArrayList;
import java.util.List;

import AbstractClass.BookLoan;
import Repositories.AllMember;
import Repositories.BookForLoan;
import model.Comic;
import model.LoanBookOrder;
import model.Member;
import model.Novel;

public class FunctionMenu {
    static List<BookLoan> listofBookLoan = BookForLoan.getAllbookLoanList();
    static List<Member> listofMember = AllMember.getAllMemberList();
    static List<LoanBookOrder> listAllBookOrder = new ArrayList<LoanBookOrder>();

    public static List<LoanBookOrder> getAllBookOrders() {
        return listAllBookOrder;
    }

    public static List<BookLoan> getAllBookForLoan() {
        return listofBookLoan;
    }

    public static List<Member> getAllMembers() {
        return listofMember;
    }

    public static LoanBookOrder getBookOrder(String orderID) {
        LoanBookOrder getBookOrder = new LoanBookOrder();
        for (LoanBookOrder loanBookOrder : listAllBookOrder) {
            if (loanBookOrder.getLoanID().equalsIgnoreCase(orderID)) {
                getBookOrder = loanBookOrder;
            }
        }
        return getBookOrder;
    }

    public static Member getMember(String memberId) {
        Member getMember = new Member();
        for (Member member : listofMember) {
            if (member.getMemberId().equalsIgnoreCase(memberId)) {
                getMember = member;
            }
        }
        return getMember;
    }

    public static BookLoan getBookLoan(String bookID) {
        List<BookLoan> getBook = new ArrayList<BookLoan>();
        for (BookLoan book : listofBookLoan) {
            if (book.getBookId().equalsIgnoreCase(bookID)) {
                getBook.add(book);
            }
        }

        return getBook.get(0);
    }

    public static String generateLoanID() {
        String getLoanID = "";
        int firstNumber = 1;
        if (listAllBookOrder.size() == 0) {
            getLoanID = "Ord-00" + firstNumber;
        } else {
            int tempNumber = listAllBookOrder.size() + 1;
            if (tempNumber % 10 == 0) {
                getLoanID = "Ord-0" + tempNumber;
            } else {
                getLoanID = "Ord-00" + tempNumber;
            }
        }
        return getLoanID;
    }

    public static void addLoanOrder(LoanBookOrder order) {
        try {
            listAllBookOrder.add(order);
            for (BookLoan book : listofBookLoan) {
                if (order.getBookloan().equals(book)) {
                    if (order.getBookloan() instanceof Novel) {
                        ((Novel) order.getBookloan()).setStock(((Novel) order.getBookloan()).getStock() - 1);
                        ((Novel) order.getBookloan()).calculateBookLoan();
                    }
                    if (order.getBookloan() instanceof Comic) {
                        ((Comic) order.getBookloan()).setStock(((Comic) order.getBookloan()).getStock() - 1);
                        ((Comic) order.getBookloan()).calculateBookLoan();
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Gagal Order ..!");
        }
    }

    public static void returnLoanBook(LoanBookOrder order) {
        try {
            listAllBookOrder.remove(order);
            for (BookLoan book : listofBookLoan) {
                if (order.getBookloan().equals(book)) {
                    if (order.getBookloan() instanceof Novel) {
                        ((Novel) order.getBookloan()).setStock(((Novel) order.getBookloan()).getStock() + 1);
                        ((Novel) order.getBookloan()).calculateBookLoan();
                    }
                    if (order.getBookloan() instanceof Comic) {
                        ((Comic) order.getBookloan()).setStock(((Comic) order.getBookloan()).getStock() + 1);
                        ((Comic) order.getBookloan()).calculateBookLoan();
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Gagal Mengembalikan Buku ..!");
        }
    }
}
