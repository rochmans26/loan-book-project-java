package model;

import AbstractClass.BookLoan;

public class Novel extends BookLoan {
    private boolean isSeries;

    public Novel() {
    }

    public Novel(boolean isSeries) {
        this.isSeries = isSeries;
    }

    public Novel(int stock, double loanPrice, boolean isSeries) {
        super(stock, loanPrice);
        this.isSeries = isSeries;
        calculateBookLoan();
    }

    public Novel(String bookId, String title, String author, double price, int stock, double loanPrice,
            boolean isSeries) {
        super(bookId, title, author, price, stock, loanPrice);
        this.isSeries = isSeries;
        if (loanPrice == 0) {
            calculateBookLoan();
        } else {
            setLoanPrice(loanPrice);
        }
    }

    @Override
    public void calculateBookLoan() {
        double result = 0.0;
        if (getStock() < STOCK_BENCHMARK) {
            result = (PERCENTAGE_STOCK_LOWER_THAN_STOCK_BENCHMARK + RATES_NOVEL) * getPrice();
        } else if (getStock() >= STOCK_BENCHMARK) {
            result = (PERCENTAGE_STOCK_HIGHER_THAN_STOCK_BENCHMARK + RATES_NOVEL) * getPrice();
        }
        setLoanPrice(result);
    }

    public boolean isSeries() {
        return isSeries;
    }

    public void setSeries(boolean isSeries) {
        this.isSeries = isSeries;
    }

}
