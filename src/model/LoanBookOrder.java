package model;

import AbstractClass.BookLoan;
import InterfaceClass.InterCalculateLoanFee;

public class LoanBookOrder implements InterCalculateLoanFee {
    private String loanID;
    private Member member;
    private BookLoan bookloan;
    private int loanDuration;
    private double loanFee;

    public LoanBookOrder() {
    }

    public LoanBookOrder(String loanID, Member member, BookLoan bookloan, int loanDuration, double loanFee) {
        this.loanID = loanID;
        this.member = member;
        this.bookloan = bookloan;
        this.loanDuration = loanDuration;
        this.loanFee = loanFee;
        calculateLoanFee();
    }

    @Override
    public void calculateLoanFee() {
        double result = 0.0;
        result = bookloan.getLoanPrice() * getLoanDuration();
        setLoanFee(result);
    }

    public String getLoanID() {
        return loanID;
    }

    public void setLoanID(String loanID) {
        this.loanID = loanID;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public BookLoan getBookloan() {
        return bookloan;
    }

    public void setBookloan(BookLoan bookloan) {
        this.bookloan = bookloan;
    }

    public int getLoanDuration() {
        return loanDuration;
    }

    public void setLoanDuration(int loanDuration) {
        this.loanDuration = loanDuration;
    }

    public double getLoanFee() {
        return loanFee;
    }

    public void setLoanFee(double loanFee) {
        this.loanFee = loanFee;
    }

}
