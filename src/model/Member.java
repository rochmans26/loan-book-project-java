package model;

import AbstractClass.Person;

public class Member extends Person {
    private String memberId;

    public Member() {
    }

    public Member(String memberId) {
        this.memberId = memberId;
    }

    public Member(String memberId, String name, String address) {
        super(name, address);
        this.memberId = memberId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @Override
    public String toString() {
        return "Member [memberId=" + memberId + ", memberName=" + getName() + ", memberAddress=" + getAddress() + "]";
    }

}
