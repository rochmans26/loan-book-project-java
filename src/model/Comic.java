package model;

import AbstractClass.BookLoan;

public class Comic extends BookLoan {
    private String genre;

    public Comic() {
    }

    public Comic(String genre) {
        this.genre = genre;
    }

    public Comic(int stock, double loanPrice, String genre) {
        super(stock, loanPrice);
        this.genre = genre;
        calculateBookLoan();
    }

    public Comic(String bookId, String title, String author, double price, int stock, double loanPrice, String genre) {
        super(bookId, title, author, price, stock, loanPrice);
        this.genre = genre;
        if (loanPrice == 0) {
            calculateBookLoan();
        } else {
            setLoanPrice(loanPrice);
        }
    }

    @Override
    public void calculateBookLoan() {
        double result = 0.0;
        if (getStock() < STOCK_BENCHMARK) {
            result = (PERCENTAGE_STOCK_LOWER_THAN_STOCK_BENCHMARK + RATES_COMIC) * getPrice();
        } else if (getStock() >= STOCK_BENCHMARK) {
            result = (PERCENTAGE_STOCK_HIGHER_THAN_STOCK_BENCHMARK + RATES_COMIC) * getPrice();
        }
        setLoanPrice(result);
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

}
