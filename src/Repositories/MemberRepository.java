package Repositories;

import java.util.Arrays;
import java.util.List;

import model.Member;

public class MemberRepository {
    public static List<Member> getAllMembers() {
        Member member1 = new Member("M-001", "Risman", "Bandung");
        Member member2 = new Member("M-002", "Budi", "Bandung");
        Member member3 = new Member("M-003", "Resti", "Kab.Bandung");
        List<Member> allMembers = Arrays.asList(member1, member2, member3);
        return allMembers;
    }
}
