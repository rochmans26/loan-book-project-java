package Repositories;

import java.util.Arrays;
import java.util.List;

import AbstractClass.BookLoan;
import model.Comic;
import model.Novel;

public class BookLoanRepository {
    public static List<BookLoan> getAllBookLoan() {
        // Member member1 = new Member("M-001", "Risman", "Bandung");
        // Member member2 = new Member("M-002", "Budi", "Bandung");
        // Member member3 = new Member("M-003", "Resti", "Kab.Bandung");

        Comic comic1 = new Comic("Comic - 001", "Uzumaki Naruto", "Masashi Kisimoto", 55000, 10, 0, "Shounen");
        Comic comic2 = new Comic("Comic - 002", "The Worst Client", "Masashi Kisimoto", 35000, 20, 0,
                "Shounen");
        Comic comic3 = new Comic("Comic - 003", "For the Sake of Dreams...!!", "Masashi Kisimoto", 35000, 15, 0,
                "Shounen");
        Comic comic4 = new Comic("Comic - 004", "Hunter X Hunter : The Day of Departure", "Yoshihiro Togashi",
                50000,
                15, 0, "Fantasy");
        Comic comic5 = new Comic("Comic - 005", "Hunter X Hunter : A Struggle in the Mist", "Yoshihiro Togashi",
                80000,
                25, 0, "Fantasy");

        Novel novel1 = new Novel("Novel - 001", "Harry Potter and the Philosopher's Stone", "J.K Rowling",
                150000, 10, 0, true);
        Novel novel2 = new Novel("Novel - 002", "Harry Potter and the Chamber of Secrets", "J.K Rowling",
                150000, 10, 0, true);
        Novel novel3 = new Novel("Novel - 003", "Harry Potter and the Prisoner of Azkaban", "J.K Rowling",
                200000, 15, 0, true);
        Novel novel4 = new Novel("Novel - 004", "Ayah Aku Berbeda", "Tere Liye",
                35000, 15, 0, false);
        Novel novel5 = new Novel("Novel - 005", "Madre", "Dee Lestari",
                80000, 10, 0, false);

        List<BookLoan> listAllBookLoan = Arrays.asList(comic1, comic2, comic3, comic4,
                comic5, novel1, novel2, novel3, novel4, novel5);

        return listAllBookLoan;
    }
}