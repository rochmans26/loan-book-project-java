package Repositories;

import java.util.ArrayList;
import java.util.List;

import model.Member;

public class AllMember {
    static List<Member> setMemberFromRepo = MemberRepository.getAllMembers();

    public static List<Member> getAllMemberList() {
        List<Member> newList = new ArrayList<Member>();
        for (Member iterable_element : setMemberFromRepo) {
            newList.add(iterable_element);
        }
        return newList;
    }
}
