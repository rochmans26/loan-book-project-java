package Repositories;

import java.util.ArrayList;
import java.util.List;

import AbstractClass.BookLoan;

public class BookForLoan {
    static List<BookLoan> setbookLoanFromRepo = BookLoanRepository.getAllBookLoan();

    public static List<BookLoan> getAllbookLoanList() {
        List<BookLoan> newList = new ArrayList<BookLoan>();
        for (BookLoan iterable_element : setbookLoanFromRepo) {
            newList.add(iterable_element);
        }
        return newList;
    }
}
